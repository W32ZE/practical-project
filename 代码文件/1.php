﻿<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf8">
		<title></title>
	</head>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.staticfile.org/vue/2.2.2/vue.min.js"></script>
	<body>
    <?php
    $dsn="mysql:host=localhost;dbname=exam";
    $db=new PDO($dsn,'root','');
    $db->query('set names utf8');
    $result=$db->query('select * from exam0');
    $result->setFetchMode(PDO::FETCH_ASSOC);?>
    <div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title" align="center">安排考试</h3>
            </div>
            <div class="panel-body">
                <table  class="table table-striped" >
                    <fieldset>
                        <legend class="text-center">共有<?php echo $result->rowCount() ?>门课程可安排考试</legend>
                        <thead>
                        <tr>
                            <th>考试科目</th>
                            <th>考试班级</th>
                            <th>考试时间</th>
                            <th>考试地点</th>
                            <th>监考老师</th>
                        </tr>
                        </thead>
                    </fieldset>
                    <tbody>
                    <form method='post' action="insert.php">
                    <?php
                    while($row=$result->fetch()){
                        echo "          
                          <tr>
	                      <td>$row[course]</td>
	                      <td>$row[class]</td>
                          <td>
                          <input type=\"date\" value=\"2015-09-24\" name=$row[course]+$row[class]+time0>
                          <input type=\"time\" value=\"13:59\" name=$row[course]+$row[class]+time1>
                          </td>
                          <td>
                          <select name=$row[course]+$row[class]+place0>
                          <option value=\"轻功\">轻工</option>
                          <option value=\"化工\">化工</option>
                          <option value=\"东教\">东教</option>
                          <option value=\"子兴\">子兴</option>
                           </select>
                           <input type='text' size='5' name=$row[course]+$row[class]+place1>
                          </td>
                          <td><input type='text' size='5' name=$row[course]+$row[class]+teacher></td>
                         </tr>
                         ";
                        $name=$row['course'];
                    }
                    ?>
                        <input type="submit" style="float: right">
                    </form>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
	</body>

</html>
